//
//
//

// Controler commands

#define IR_KEY_PLUS       1
#define IR_KEY_LESS       0
#define IR_KEY_OFF        2
#define IR_KEY_ON         3
#define IR_KEY_WHITE      7
#define IR_KEY_FLASH     11
#define IR_KEY_STROBE    15
#define IR_KEY_FADE      19
#define IR_KEY_SMOOTH    23

#define IR_KEY_RED        4
#define IR_KEY_GREEN      5
#define IR_KEY_BLUE       6
#define IR_KEY_7          8
#define IR_KEY_8          9
#define IR_KEY_9         10
#define IR_KEY_4         12
#define IR_KEY_5         13
#define IR_KEY_6         14
#define IR_KEY_1         16 
#define IR_KEY_2         17
#define IR_KEY_3         18
#define IR_KEY_0         20
#define IR_KEY_F1        21
#define IR_KEY_F2        22


class Controler {
public:

  

  Controler () { } ;

  ~Controler () { } ;

  ctl_command ( int p_command ) {

    db.dbg_enter ("ctl_command") ;
    db.dbg_debug ("command", p_command ) ;

    switch ( p_command ) {
        case IR_KEY_PLUS      : { myRgbLed.rgb_lighten() ; break ; }
        case IR_KEY_LESS      : { myRgbLed.rgb_darken()  ; break ; }
        case IR_KEY_OFF       : { myRgbLed.rgb_deactivate () ; break ; }
        case IR_KEY_ON        : { myRgbLed.rgb_activate () ; break ; }
        case IR_KEY_WHITE     : { myRgbLed.rgb_select ( true, true, true ) ;
                                  myRgbLed.rgb_set ( RGB_MAX, RGB_MAX, RGB_MAX ) ; 
                                  break ; 
                                }
        case IR_KEY_FLASH     : { break ; }
        case IR_KEY_STROBE    : { break ; }
        case IR_KEY_FADE      : { break ; }
        case IR_KEY_SMOOTH    : { break ; }
        case IR_KEY_RED       : { if ( myRgbLed.rgb_selected(RGB_RED) ) {
                                    myRgbLed.rgb_set ( RGB_RED, RGB_MAX ) ;
                                  } else {
                                    myRgbLed.rgb_select ( true, false, false ) ;
                                  }
                                  break ; 
                                }
        case IR_KEY_GREEN     : { if ( myRgbLed.rgb_selected(RGB_GREEN) ) {
                                    myRgbLed.rgb_set ( RGB_GREEN, RGB_MAX ) ;
                                  } else {
                                    myRgbLed.rgb_select ( false, true, false ) ;
                                  }
                                  break ; 
                                }
        case IR_KEY_BLUE      : { if ( myRgbLed.rgb_selected(RGB_BLUE) ) {
                                    myRgbLed.rgb_set ( RGB_BLUE, RGB_MAX ) ;
                                  } else {
                                    myRgbLed.rgb_select ( false, false, true ) ;
                                  }
                                  break ; 
                                }
        case IR_KEY_0         : { myRgbLed.rgb_set ( 00 ) ;
                                  break ; 
                                }
        case IR_KEY_1         : { myRgbLed.rgb_set ( 10 ) ;
                                  break ; 
                                }
        case IR_KEY_2         : { myRgbLed.rgb_set ( 20 ) ;
                                  break ; 
                                }
        case IR_KEY_3         : { myRgbLed.rgb_set ( 30 ) ;
                                  break ; 
                                }
        case IR_KEY_4         : { myRgbLed.rgb_set ( 40 ) ;
                                  break ; 
                                }
        case IR_KEY_5         : { myRgbLed.rgb_set ( 50 ) ;
                                  break ; 
                                }
        case IR_KEY_6         : { myRgbLed.rgb_set ( 60 ) ;
                                  break ; 
                                }
        case IR_KEY_7         : { myRgbLed.rgb_set ( 70 ) ;
                                  break ; 
                                  }
        case IR_KEY_8         : { myRgbLed.rgb_set ( 80 ) ;
                                  break ; 
                                }
        case IR_KEY_9         : { myRgbLed.rgb_set ( 90 ) ;
                                  break ; 
                                }
        case IR_KEY_F1        : { break ; }
        case IR_KEY_F2        : { break ; }
    }

    db.dbg_leave() ;
    
  }
} ;


//
// end of file
//

