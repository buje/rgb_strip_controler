/* comboDump.ino Example sketch for IRLib2
 *  Illustrate how to create a custom decoder using only the protocols
 *  you wish to use.
 */
#include <IRLibDecodeBase.h> // First include the decode base
#include <IRLib_P01_NEC.h>   // Now include only the protocols you wish
                             // to actually use. The lowest numbered
                             // must be first but others can be any order.
#include <IRLibCombo.h>      // After all protocols, include this
// All of the above automatically creates a universal decoder
// class called "IRdecode" containing only the protocols you want.
// Now declare an instance of that decoder.

// Include a receiver either this or IRLibRecvPCI or IRLibRecvLoop
#include <IRLibRecv.h> 

#define IR_KEY_NONE    0xFE
#define IR_KEY_UNKNOWN 0xFF

class IrKeyPad {
public:
IRdecode * myDecoder;
int last_key = IR_KEY_NONE ;
unsigned int last_code = 0 ;


IRrecv * myReceiver;  //pin number for the receiver


  IrKeyPad( int p_pin_no) {
    myDecoder = new IRdecode();
    myReceiver = new IRrecv ( p_pin_no );
    myReceiver->enableIRIn(); // Start the receiver
    
  }

  ~IrKeyPad() {
    delete myDecoder ;
    delete myReceiver ;
    
  }

  private:
  //unsigned int ir_key_hex_codes[] = {
  //    0xF700FF, 0xF7807F, 0xF740BF, 0xF7C03F, 
  //    0xF720DF, 0xF7A05F, 0xF7609F, 0xF7E01F, 
  //    0xF710EF, 0xF7906F, 0xF750AF, 0xF7D02F, 
  //    0xF730CF, 0xF7B04F, 0xF7708F, 0xF7F00F, 
  //    0xF708F7, 0xF78877, 0xF748B7, 0xF7C837, 
  //    0xF728D7, 0xF7A857, 0xF76897, 0xF7E817
  //
  //  };
  
  int hex_code_to_key() {
    switch (last_code) {
      case 0xF700FF : return  0 ; break ;
      case 0xF708F7 : return 16 ; break ;
      case 0xF710EF : return  8 ; break ;
      case 0xF720DF : return  4 ; break ;
      case 0xF728D7 : return 20 ; break ;
      case 0xF730CF : return 12 ; break ;
      case 0xF740BF : return  2 ; break ;
      case 0xF748B7 : return 18 ; break ;
      case 0xF750AF : return 10 ; break ;
      case 0xF7609F : return  6 ; break ;
      case 0xF76897 : return 22 ; break ;
      case 0xF7708F : return 14 ; break ;
      case 0xF7807F : return  1 ; break ;
      case 0xF78877 : return 17 ; break ;
      case 0xF7906F : return  9 ; break ;
      case 0xF7A05F : return  5 ; break ;
      case 0xF7A857 : return 21 ; break ;
      case 0xF7B04F : return 13 ; break ;
      case 0xF7C03F : return  3 ; break ;
      case 0xF7C837 : return 19 ; break ;
      case 0xF7D02F : return 11 ; break ;
      case 0xF7E01F : return  7 ; break ;
      case 0xF7E817 : return 23 ; break ;
      case 0xF7F00F : return 15 ; break ;
      default  : return IR_KEY_UNKNOWN ;  
  
    }
  }

public :

  void activate (void) {
    myReceiver->enableIRIn();
  }

  int ir_key(void) {
    
    if (myReceiver->getResults()) { 
      myDecoder->decode();           //Decode it

      last_code = myDecoder->value ;
    
      last_key = hex_code_to_key();
      
      myReceiver->enableIRIn();      //Restart receiver
     
    } else {
      last_key = IR_KEY_NONE ;
    }

    return last_key  ;
  }
  
} ;

// end of file


