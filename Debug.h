
//
// Debug and trace utility
//
// Output messages and values to Serial console
//


// Special characters and strings
//
const char star   = '*'   ;
const char space  = ' '   ;
const char colon []  = " : " ;


class Debug {
public:

  // indentation and subroutine depth level
  
  int dbg_level = 0 ;


  // Constructor and destructor 
  Debug  () {}
  
  ~Debug () {}

  //
  // Init Serial console
  //
  dbg_init (void ) {
    Serial.begin(9600);
  }

  //
  // output p_nb times the char p_char
  //
  void dbg_chars(char p_char, byte p_nb ) {
    for ( byte i = 0 ; i < p_nb ; i++ ) { Serial.print(p_char) ; }
  }


  //
  // Enter a trace sublevel
  //
  void dbg_enter( const char * msg ) {

    // insert a blank line
    Serial.println("");
    
    // increase indentation level
    dbg_level ++ ;

    // insert a separation line
    dbg_chars(space, dbg_level);
    dbg_chars(star , 40 ) ;
    // insert a blank line
    Serial.println("");

    // and output enter message given as a parameter
    dbg_debug(msg) ;
    
  }

  // leave a trace sublevel
  void dbg_leave ( void ) {

    // insert a separation line
    dbg_chars(space, dbg_level);
    dbg_chars(star, 40 ) ;

    // insert a blank line
    Serial.println("");

    // decrement indentation level
    dbg_level -- ;
    
  }

  // output a message
  dbg_debug( const char * p_str ) {

    // output indentation
    dbg_chars (space , dbg_level );

    // ... and message
    Serial.println(p_str);
  }

  // output a message and an int value
  dbg_debug (const char * p_str, int p_int ) {

    // output indentation ...
    dbg_chars ( space, dbg_level ) ;

    // ... then message ...
    Serial.print(p_str);

    // ... then a colon ...
    Serial.print(colon) ;

    // then the value in hex
    Serial.println(p_int);
  }

  
} ;

//
// end of file
//

