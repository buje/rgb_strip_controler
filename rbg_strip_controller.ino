/* comboDump.ino Example sketch for IRLib2
 *  Illustrate how to create a custom decoder using only the protocols
 *  you wish to use.
 */

#define IR_PIN_NO    2

#define RGB_PIN_RED     9
#define RGB_PIN_GREEN  10
#define RGB_PIN_BLUE   6


#include "Debug.h"
Debug db ;


#include "RgbLed.h"
RgbLed  myRgbLed ( RGB_PIN_RED, RGB_PIN_GREEN, RGB_PIN_BLUE );

#include "IrKeyPad.h"
IrKeyPad myIrKeyPad (IR_PIN_NO) ;

#include "Controler.h"
Controler ctrl ;


void setup() {
  db.dbg_init() ;
  myIrKeyPad.activate(); // Start the receiver
  // myRgbLed.rgb_deactivate() ;
  db.dbg_debug("Ready to receive IR signals");
}



void loop() {
  int key ;

  key = myIrKeyPad.ir_key();

  if ( (key != IR_KEY_NONE ) && (key != IR_KEY_UNKNOWN ) ) {

      ctrl.ctl_command ( key ) ;
    
  }
}

