//
// RGB LED strip controler class
//

//
//This class controls a RGB led strip.
//Its constructor expects arduino pwm pins that will control the red, green and blue 
//
//Each colors is PWM ajusted via a pnl2806
//

// index values for RGB colors
//
#define RGB_RED    0
#define RGB_GREEN  1
#define RGB_BLUE   2

// number of channels to be controlled ( R, G and B --> 3 channels)
// All arrays will be sized to this value
//
#define RGB_PIN_COUNT  3

// default color value for each channel is 100%
//
#define DEFAULT_COLOR_VALUE 100

// RGM min and max values in % of max PWM value
//
#define RGB_MIN   0
#define RGB_MAX 100

// PWM min and max values
// A 8bit PWM channel gives 256 values
//  
#define PWM_MIN   0
#define PWM_MAX 250

// RGB deltal when increasing or decreasing color levels
//
#define RGB_DELTA     5


//
// The main class
//

class RgbLed {

public:

  // All arrays are 3 values long, Red, then Green, then Blue

  // Arduino pin numbers for each color channel
  //
  int rgb_pin [ RGB_PIN_COUNT ] = { -1, -1, -1 } ;

  // level values for each RGB channel
  byte rgb_value [ RGB_PIN_COUNT ] ;

  // which channels are selected for level setting
  bool rgb_current [ RGB_PIN_COUNT ] = { true, true, true } ;
  bool rgb_all = true ;

  // Tells if RGB strip is on or off
  bool rgb_activated = false ;

  // constructor
  RgbLed ( int p_pin_Red , int p_pin_Green , int p_pin_Blue ) {

    // store arduino pin numbers
    rgb_pin [ RGB_RED   ] = p_pin_Red ;
    rgb_pin [ RGB_GREEN ] = p_pin_Green ;
    rgb_pin [ RGB_BLUE  ] = p_pin_Blue ;

    //
    // Set each arduino pin as an output
    // and set each channle to devault value
    //
    for (int i = 0 ; i < RGB_PIN_COUNT ; i++ ) { 
      // pinMode ( rgb_pin [ i ] , OUTPUT ) ; 
      rgb_value [ i ] = DEFAULT_COLOR_VALUE ;
    }
    
  }

  // Destructor
  //
  ~RgbLed (  ) { }


  //
  // This method sets each PWM pins to the corresponding values
  //
  void rgb_display ( void ) {

    db.dbg_enter("rgb_display") ;
    db.dbg_debug("rgb_activated", rgb_activated ) ;
    
    // exit if not activated
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 
    
    db.dbg_debug("Setting each RGB values ") ;

    // loop through channels
    //
    for (int i = 0 ; i < RGB_PIN_COUNT ; i++ ) {

      // A bit of debug messages
      db.dbg_debug("color ", i );
      db.dbg_debug("value", (int)(rgb_value [ i ]));
      db.dbg_debug("pwm value", rgb_value [ i ] * PWM_MAX / RGB_MAX );

      // output value
      analogWrite ( (int)(rgb_pin [ i ] ) , (int) (rgb_value [ i ]) ) ; // * PWM_MAX / RGB_MAX ) ) ;
      
    }
    db.dbg_leave();
    
  }

  //
  // activate rgb led strip
  // each channel will be output
  //
  void rgb_activate ( void ) {
    db.dbg_enter("rgb_activate") ;
    
    rgb_activated = true ;

    db.dbg_debug("is activated", rgb_activated);

    // output new values
    rgb_display() ;

    db.dbg_leave() ;
    
  } 

  //
  // deactivate led strip
  // PWM will be set to 0 for each channel but RGB values are preserved
  //
  void rgb_deactivate ( void ) {

    db.dbg_enter("rgb_deactivate") ;
    
    rgb_activated = false ;

    db.dbg_debug("is activated", rgb_activated);

    //
    // loop through RGB channels ...
    for (int i = 0 ; i < RGB_PIN_COUNT ; i++ ) {

      db.dbg_debug("Setting to 0 channel", i ) ;
      // ... and set them to 0
      
      analogWrite ( rgb_pin [ i ] , 0 ) ;
      
    }

    db.dbg_leave() ;
    
  }


  //
  // Set RBG values for each channel
  //
  void rgb_set ( byte p_red, byte p_green, byte p_blue ) {

    db.dbg_enter("rgb_set");
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 

    db.dbg_debug("Setting red to ", p_red );
    rgb_value [ RGB_RED   ] = p_red ;
    db.dbg_debug("Setting green to ", p_green );
    rgb_value [ RGB_GREEN ] = p_green ;
    db.dbg_debug("Setting blue to ", p_blue );
    rgb_value [ RGB_BLUE  ] = p_blue ;    

    // output new values
    rgb_display() ;

    db.dbg_leave();
  }

  // 
  // Set a channel to a specific RGB value
  //
  void rgb_set ( int p_channel, byte p_value ) {

    db.dbg_enter("rgb_set");
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 

    db.dbg_debug("Setting channel", p_channel) ;
    db.dbg_debug("  to", p_value);

    // store new value to choosen channel
    rgb_value [ p_channel   ] = p_value ;

    rgb_display() ;

    db.dbg_leave() ;
    
  }

  //
  // Set selected channels to a specific same value
  //   all channels in white mode
  //   one channel if a color has been selected
  //
  void rgb_set (byte p_level ) {

    db.dbg_enter("rgb_set");
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 

    db.dbg_debug("Iterate through channels" ) ;
    
    for ( int i = 0 ; i < RGB_PIN_COUNT ; i++ ) {

      db.dbg_debug("channel", i );
      db.dbg_debug("  selected ", rgb_current [ i ] );
      db.dbg_debug ( "  old value ", rgb_value [ i ] );
      
      if ( rgb_current [ i ] ) {
        
        db.dbg_debug("  set value to ", p_level );
        
        rgb_value [ i  ] = p_level ;

        db.dbg_debug ( "  new value ", rgb_value [ i ] );
        
      }
    }

    // output new values
    rgb_display() ;

    db.dbg_leave() ;
  }

  void rgb_adjust ( byte p_delta ) {

    db.dbg_enter("rgb_adjust");
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 

    db.dbg_debug("Iterate through channels" ) ;
    for ( int i = 0 ; i < RGB_PIN_COUNT ; i++ ) {
      
       db.dbg_debug ( "channel ", i ) ;
       db.dbg_debug ( "  old value ", rgb_value [ i ] );

       // Adjust channel value
       rgb_value [ i ] = rgb_value [ i ] + p_delta  ;
       
       db.dbg_debug ( "new value ", rgb_value [ i ] );

       // Correct if necessary
       if ( rgb_value [ i ] < RGB_MIN ) { rgb_value [ i ] = RGB_MIN ; }
       if ( rgb_value [ i ] > RGB_MAX ) { rgb_value [ i ] = RGB_MAX ; }
       db.dbg_debug ( "  corrected value ", rgb_value [ i ] );
       
       
    } 

    // output new values
    rgb_display() ;

    db.dbg_leave();
      
  }

  //
  // return channel value
  //
  byte rgb_get ( byte p_color ) {
    
    return rgb_value [ p_color ] ;
    
  }

  //
  // level up all channels
  //
  void rgb_lighten () {

    db.dbg_enter("rgb_lighten");
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 

    // adjust all channel levels
    rgb_adjust ( + RGB_DELTA ) ;

    db.dbg_leave() ;
    
  }

  //
  // level up all channels
  //
  void rgb_darken () {

    db.dbg_enter("rgb_darken");
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 

    // adjust all channel levels
    rgb_adjust ( - RGB_DELTA ) ;

    db.dbg_leave() ;
    
  }


  //
  // Select a set of channels, R or G or B or all
  //
  void rgb_select ( bool p_red, bool p_green, bool p_blue ) {

    db.dbg_enter("rgb_select");
    
    if ( ! rgb_activated ) { db.dbg_leave() ; return ; } 

    // check if in white mode (all channels are active at once), 
    //   or in color mode 
    rgb_all = ( p_red && p_green && p_blue ) ;

    // set selection flags
    db.dbg_debug("  RED", p_red);
    rgb_current [ RGB_RED   ] = p_red   ;
    db.dbg_debug("  GREEN", p_green);
    rgb_current [ RGB_GREEN ] = p_green ;
    db.dbg_debug("  BLUE", p_blue);
    rgb_current [ RGB_BLUE  ] = p_blue  ;

    db.dbg_leave();
    
  }

  // 
  // return true if channel is selected
  //
  bool rgb_selected ( int p_channel ) {

    if ( rgb_all ) { return false ; }
    
    return rgb_current [ p_channel ] ;
  }

  virtual void loop ( void ) { return ;  }

  
  
};

//
// end of file
//

